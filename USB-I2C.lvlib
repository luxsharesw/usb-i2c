﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!**!!!*Q(C=\&gt;3R=?*!&amp;-&lt;RTT=8_-Y2I&lt;-\:C[Y_,6!#\4Q:KY#5I?U]/5YI144QBNX1!NK!@_V7G3=G/1]Y]!3#_,&lt;X&lt;=`*#'J&lt;&lt;@3&gt;ZWO&lt;5`P&lt;P1T9HS.2XXU?$4VT@WH+:]`H]\Z[`SJRE8SJPY]YXQUT\_=_LL_H&amp;`T8^PU\P&lt;6`^8`O@OPX^Y0V`]%(\N*.UV++ZJJ39PW[UZ&amp;8O2&amp;8O2&amp;8O1G.\H*47ZSES&gt;ZEC&gt;ZEC&gt;ZEA&gt;ZE!&gt;ZE!&gt;ZE,?&gt;8/1C&amp;TFHJ6C]7+AI7B1I"E.2]6:Y#E`B+4R]6?%J0)7H]"1?BKDQ&amp;*\#5XA+$^.5?!J0Y3E]B9&gt;346*N*]&gt;4?#APYT%?YT%?YW&amp;*'9]"G-6-96-%BESH/4!?YT%?$G5]RG-]RG-]&gt;-NYD-&gt;YD-&gt;YG.,/CJNGX-HR5%;**`%EHM34?#CNR*.Y%E`C34QMJ]34?"*%MG"3()+33=G!Z%PC34R]+0%EHM34?")08?U+:4MTIW&lt;=S@%%HM!4?!*0Y+'%!E`A#4S"*`"16I%H]!3?Q".Y7%K"*`!%HA!3,-LS#II&amp;%Y."12"Y?,7\*&gt;J6=J.%W`N@=\J2V7^!^2N,`9:2PR(5,\$[B6/`)/IH7PU%KJ]9^2_M`E05A?I,KR&gt;5([C"^S0N1.P4NL1.&lt;5V&lt;U:;UR4DV0Q]=BE((YV'(QU(\`6\&lt;\6;&lt;T5&lt;L^6KLV5L,Z6+,R7*_7PVCH\;&lt;]X0JA?0&gt;=(@`_/0W=@@]\`@O\]]`@L\TG*`\0[*.T[80]'T5.ZUO(P/=IR&gt;WS]:1!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Item Name="Palette" Type="Folder">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="USB-I2C.mnu" Type="Document" URL="../USB-I2C.mnu"/>
		<Item Name="USB-I2C Example.mnu" Type="Document" URL="../USB-I2C Example.mnu"/>
	</Item>
	<Item Name="USB-I2C Create.lvclass" Type="LVClass" URL="../USB-I2C Create/USB-I2C Create.lvclass"/>
	<Item Name="USB-I2C.lvclass" Type="LVClass" URL="../USB-I2C/USB-I2C.lvclass"/>
	<Item Name="VI Tree.vi" Type="VI" URL="../VI Tree.vi"/>
</Library>
