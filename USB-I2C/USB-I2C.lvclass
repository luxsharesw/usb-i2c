﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="17008000">
	<Property Name="NI.Lib.ContainingLib" Type="Str">USB-I2C.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../USB-I2C.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!**!!!*Q(C=\&gt;3R=?*!&amp;-&lt;RTT=8_-Y2I&lt;-\:C[Y_,6!#\4Q:KY#5I?U]/5YI144QBNX1!NK!@_V7G3=G/1]Y]!3#_,&lt;X&lt;=`*#'J&lt;&lt;@3&gt;ZWO&lt;5`P&lt;P1T9HS.2XXU?$4VT@WH+:]`H]\Z[`SJRE8SJPY]YXQUT\_=_LL_H&amp;`T8^PU\P&lt;6`^8`O@OPX^Y0V`]%(\N*.UV++ZJJ39PW[UZ&amp;8O2&amp;8O2&amp;8O1G.\H*47ZSES&gt;ZEC&gt;ZEC&gt;ZEA&gt;ZE!&gt;ZE!&gt;ZE,?&gt;8/1C&amp;TFHJ6C]7+AI7B1I"E.2]6:Y#E`B+4R]6?%J0)7H]"1?BKDQ&amp;*\#5XA+$^.5?!J0Y3E]B9&gt;346*N*]&gt;4?#APYT%?YT%?YW&amp;*'9]"G-6-96-%BESH/4!?YT%?$G5]RG-]RG-]&gt;-NYD-&gt;YD-&gt;YG.,/CJNGX-HR5%;**`%EHM34?#CNR*.Y%E`C34QMJ]34?"*%MG"3()+33=G!Z%PC34R]+0%EHM34?")08?U+:4MTIW&lt;=S@%%HM!4?!*0Y+'%!E`A#4S"*`"16I%H]!3?Q".Y7%K"*`!%HA!3,-LS#II&amp;%Y."12"Y?,7\*&gt;J6=J.%W`N@=\J2V7^!^2N,`9:2PR(5,\$[B6/`)/IH7PU%KJ]9^2_M`E05A?I,KR&gt;5([C"^S0N1.P4NL1.&lt;5V&lt;U:;UR4DV0Q]=BE((YV'(QU(\`6\&lt;\6;&lt;T5&lt;L^6KLV5L,Z6+,R7*_7PVCH\;&lt;]X0JA?0&gt;=(@`_/0W=@@]\`@O\]]`@L\TG*`\0[*.T[80]'T5.ZUO(P/=IR&gt;WS]:1!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.11</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.CoreWirePen" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!6+0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z1:7Y],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5T-DY.#DR/97VF0E:P=G6H=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0D-T/$%X.4E],V:B&lt;$Y.#DQP64-S0AU+0&amp;5T-DY.#DR/97VF0E*B9WNH=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0D%W.T=X-D%V0#^797Q_$1I],V5T-DY.#DR$&lt;(6T&gt;'6S0AU+0%ZB&lt;75_2GFM&lt;#"1982U:8*O0#^/97VF0AU+0%ZV&lt;56M&gt;(-_/$QP4H6N27RU=TY.#DR6/$Y.#DR/97VF0F*P&gt;S!Q0#^/97VF0AU+0&amp;:B&lt;$YR/45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-4QP4G&amp;N:4Y.#DR797Q_-4EV0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$)],UZB&lt;75_$1I]6G&amp;M0D%Z.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!T0#^/97VF0AU+0&amp;:B&lt;$YR/45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.$QP4G&amp;N:4Y.#DR797Q_-4EV0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$5],UZB&lt;75_$1I]6G&amp;M0D%Z.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!W0#^/97VF0AU+0&amp;:B&lt;$YR/45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.TQP4G&amp;N:4Y.#DR797Q_-4EV0#^797Q_$1I],V5Y0AU+0#^$&lt;(6T&gt;'6S0AU+0%ER.DY.#DR/97VF0F&gt;J:(2I0#^/97VF0AU+0&amp;:B&lt;$YR0#^797Q_$1I],UER.DY.#DR&amp;6TY.#DR/97VF0EVP:'5],UZB&lt;75_$1I]1WBP;7.F0E.P=(E],U.I&lt;WFD:4Y.#DR$;'^J9W5_4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_28BD&lt;(6T;8:F)%^S0#^$;'^J9W5_$1I]1WBP;7.F0E*J&gt;#"$&lt;'6B=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A1W^Q?4QP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_4G^S)%6Y9WRV=WFW:3"0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A1GFU)%.M:7&amp;S0#^$;'^J9W5_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP26=_$1I]25Q_$1I]4G&amp;N:4Z4&gt;(FM:4QP4G&amp;N:4Y.#DR$;'^J9W5_5W^M;71],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%&lt;X1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;#"%&lt;X1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;#"%&lt;X1A2'^U0#^$;'^J9W5_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP25Q_$1I]25Q_$1I]4G&amp;N:4Z';7RM)&amp;*V&lt;'5],UZB&lt;75_$1I]1WBP;7.F0E6W:7YA4W2E0#^$;'^J9W5_$1I]1WBP;7.F0F&gt;J&lt;G2J&lt;G=],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DR&amp;4$Y.#DR/97VF0E6O:#"$98"T0#^/97VF0AU+0%.I&lt;WFD:4Z%:7:B&gt;7RU0#^$;'^J9W5_$1I]1WBP;7.F0E:M981],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DQP1WRV=X2F=DY.#A!!!!!</Property>
	<Property Name="NI.LVClass.EdgeWirePen" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!6+0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z1:7Y],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5T-DY.#DR/97VF0E:P=G6H=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0D-T/$%X.4E],V:B&lt;$Y.#DQP64-S0AU+0&amp;5T-DY.#DR/97VF0E*B9WNH=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0D%W.T=X-D%V0#^797Q_$1I],V5T-DY.#DR$&lt;(6T&gt;'6S0AU+0%ZB&lt;75_2GFM&lt;#"1982U:8*O0#^/97VF0AU+0%ZV&lt;56M&gt;(-_/$QP4H6N27RU=TY.#DR6/$Y.#DR/97VF0F*P&gt;S!Q0#^/97VF0AU+0&amp;:B&lt;$YR-D9],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-4QP4G&amp;N:4Y.#DR797Q_-4)W0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$)],UZB&lt;75_$1I]6G&amp;M0D%S.DQP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!T0#^/97VF0AU+0&amp;:B&lt;$YR-D9],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.$QP4G&amp;N:4Y.#DR797Q_-4)W0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$5],UZB&lt;75_$1I]6G&amp;M0D%S.DQP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!W0#^/97VF0AU+0&amp;:B&lt;$YR-D9],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.TQP4G&amp;N:4Y.#DR797Q_-4)W0#^797Q_$1I],V5Y0AU+0#^$&lt;(6T&gt;'6S0AU+0%ER.DY.#DR/97VF0F&gt;J:(2I0#^/97VF0AU+0&amp;:B&lt;$YT0#^797Q_$1I],UER.DY.#DR&amp;6TY.#DR/97VF0EVP:'5],UZB&lt;75_$1I]1WBP;7.F0E.P=(E],U.I&lt;WFD:4Y.#DR$;'^J9W5_4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_28BD&lt;(6T;8:F)%^S0#^$;'^J9W5_$1I]1WBP;7.F0E*J&gt;#"$&lt;'6B=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A1W^Q?4QP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_4G^S)%6Y9WRV=WFW:3"0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A1GFU)%.M:7&amp;S0#^$;'^J9W5_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP26=_$1I]25Q_$1I]4G&amp;N:4Z4&gt;(FM:4QP4G&amp;N:4Y.#DR$;'^J9W5_5W^M;71],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%&lt;X1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;#"%&lt;X1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;#"%&lt;X1A2'^U0#^$;'^J9W5_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP25Q_$1I]25Q_$1I]4G&amp;N:4Z';7RM)&amp;*V&lt;'5],UZB&lt;75_$1I]1WBP;7.F0E6W:7YA4W2E0#^$;'^J9W5_$1I]1WBP;7.F0F&gt;J&lt;G2J&lt;G=],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DR&amp;4$Y.#DR/97VF0E6O:#"$98"T0#^/97VF0AU+0%.I&lt;WFD:4Z%:7:B&gt;7RU0#^$;'^J9W5_$1I]1WBP;7.F0E:M981],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DQP1WRV=X2F=DY.#A!!!!!</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!#VM5F.31QU+!!.-6E.$4%*76Q!!+2Q!!!21!!!!)!!!+0Q!!!!C!!!!!AV65U)N34*$,GRW&lt;'FC$V641CV*-E-O&lt;(:D&lt;'&amp;T=Q!!!!!!I"=!A!!!-!!!+!!!!!!!!!1!!Q!]!,Q!(U#!!A!!!!!"!!%!"P````]!!!!!!!!!!!!!!!"R:/F1`^233\5^Y8RZ43U+!!!!$!!!!"!!!!!!XX?K*XPL1%;?^1[J*:"OW^1&gt;D.G0!,)%[9!*G/TY1HY!!"!!!!!!!)$O-N=\8X^0EDY_]#"-A[M"!!!!`````^1&gt;D.G0!,)%[9!*G/TY1HY!!!!1T`&gt;KKJ^C/@-^QJVM],\&lt;C1!!!!1!!!!!!!!"EQ!"4&amp;:$1Q!!!!1!!F:*4%)!!!!!5&amp;2)-!!!!!5!!1!"!!!!!!)!!F:*1U-!!!!!!2F$&lt;WZG;7&gt;V=G&amp;U;7^O,5.M&gt;8.U:8)O9X2M5&amp;2)-!!!!#A!!1!%!!!(1W^O&gt;(*P&lt;"F$&lt;WZG;7&gt;V=G&amp;U;7^O,5.M&gt;8.U:8)O9X2M!!!!!A!"`Q!!!!%!!1!!!!!!#Q!!!!!!!!!!!!!!!!!!!!!!!F:*1U-!!!!!!B&amp;/36^-6E.P&lt;G:J:SZM&gt;GRJ9B:$&lt;WZG;7=A2'&amp;U93"3:7:/&gt;7UO9X2M!&amp;"53$!!!!!W!!!!"!=]&gt;GFM;7)_"V6U;7RJ&gt;(E+9W^O:GFH,GRM9B:$&lt;WZG;7=A2'&amp;U93"3:7:/&gt;7UO9X2M!!!!!!!#1A!!!!!!!F:*1U-!!!!!!!!"&amp;V"B=X.X&lt;X*E)%RF&gt;G6M,56O&gt;7UO9X2M5&amp;2)-!!!!#9!!1!%!!!(1W^O&gt;(*P&lt;"&gt;198.T&gt;W^S:#"-:8:F&lt;#V&amp;&lt;H6N,G.U&lt;!!!!!)!!`]!!!!"!!%!!!!!!!M!!!!!!!!!!!!!!!!!!!!!!!-!!!!!!A!%!!!!!!!D!!!!'HC=9W"H9'VAO-!!R)Q/4!V-#5$7"Q9'DA!'!%J4":I!!!!!&amp;!!!!"BYH'0A9O"BY-#!$!!&amp;%A"@!!!!4Q!!!4VYH'.AQ!4`A1")-4)Q-!M";29U=4!.9V-4Y$)8FVV1=7;I'VE8Y$/]'EUP!Q04(C$."$)!;J!K2)LJ,2#@1.@0$[5@))E"!/]5+;U!!!!!$!!"6EF%5Q!!!!!!!Q!!!&lt;Q!!!/]?*T&lt;Q-D!E'FM9=&lt;!R-$!$'3,-T1Q*/?HJ0)S!0E-%+!$9V!!!K$G;;'*'RYYH!9%?PTS,7"_]RO?&lt;B=6A?9;&amp;1GG5J&amp;O(R724B]6FEY7F2&gt;``P``XXS%ZX#X2]ZR2RO1WGY/I0BR&amp;R5/%!&gt;)MY$I`Y%:)&amp;7IZMFU!G7"N!33"LC"+09(!&amp;6R.&amp;1I-Z3Q'"[)/HS]Q912YF#9%[+QO:&gt;Y]ZP@=!!^*8$Q)5NXIQ;1XTM22!+&amp;?$J$/#3/OX$IC!(ZD#&gt;!"H&lt;SQ(T.!@&gt;0'-C!%B7"4B/123S--)O[W9Y\;)$$Q5%%1G6!K!I)61#C&gt;I"&gt;=)1D\D!]`.?_PL?,&amp;5CT)=7*!R!XA"B-K&amp;C0A:'"%=RE:&amp;A,67M$:$."R7"R#W)L1)/.E=%?LO=W6&amp;Y$S2Q82JA?B,JK*(=QA=VA:0D$!$-0;"^54Q05X3!R8[$9!3A\"-C?!'6(!^E@I/QE)&amp;M!SMY%MAU9)?Q]+"NM'1.OWNH@R25JG-$Z!J9V4)%Y/&lt;@!Q%#PON2**^J1J^1J6C&gt;9J\L52Q?-;E&amp;CQ&lt;&amp;ADJ//EUZ2GEZKO)W283V1HQF)&lt;U&amp;S';F;!;3*K&gt;=!!!*`!!!$F(C=&gt;60@3R22&amp;,YX,\,+UI69SEAM9BY3"JMVEN5&gt;;JT7.5R-8&amp;0"L!&gt;T)4$[.1/F\C:-!YW8_\&lt;^#\XV%-130?W[I5:P2A2+)E(1.AF2%$U%WTGTCSZ2FTF]=_`ZTLHH@(0'))2=0R8L)AW%.&amp;"#$J&amp;&amp;-HXTWASJ7U9$_?_[F"I@=\YGR+ASY9UKBEAIF\W%IC]V^Y2YLGC@E!05'V2#9);"$D&amp;#X88LM/A5"Y%@!^/=&amp;?;X]HSRX,+0E.7%IG&amp;?Q"BC&gt;$X[5]4=CH6%H"5HO^^Z&lt;T/.XCQ%_MYK;`_#]8\&lt;,HH40Z!/6A=`_D#ILT^ZQ&gt;X-BJ@W^X$O0I-'`2VXH??7L2$0,Z?@Q!'5(J)$$#O5ZR&amp;U-=#%Q;";ZI76T\]LF9LTC`*=A&lt;NXKXR$GOAWB+8I=CC)A;U#?%S-M0A9AQPM#4H-I$R&amp;XG;?"9Z\,(Y@(#7\8]YR&lt;Q%=793[C#YZ(%92H7)Y+--)OQ7L87CC%&lt;+XAE61+B5,XY(7?,Z5`B4A=PF$6&lt;*)4&lt;L71)W#NS+;X.=A8&lt;@AX57P".).1K)X+.U;ZP/&lt;&gt;]G&lt;)*-C'A5(#P09&lt;N`W!L$B8J$RQ4:]XIB&lt;!#HMW8`,M.@-7,6^I^K_PG3QHF['E:X3J"AFBX!S^-5M*@RR;8%/9?82&gt;WV$W\,;I#@H'`7&lt;;M,H#][LR*7LEV-F=0A`Z$#NJK;97BAUXIMU_XWN)*F%U.0#:/FYEA5:VFZ1V+KP[$_PH^_H`N:,&gt;.$[?1=\4FI)D'.A(WPH_,Z"^TCYTFV-^0X^LS$F..DUD6O;VD&amp;PG_JE6,8.+47FTNO$;P"E]#QV&amp;7R-V64PJ.7:=&lt;XT4!&lt;C`A$Y?B%_!!!!!A]!!!.-?*RV5FVL%U%5H9F87'5RCK#0PMTD5B).)97AG_W%L,,;VFB4L&amp;%B\9*@_%%C3+U7VI6-R`U0`A(`1&amp;_-_"&amp;]%PM$@"&lt;KAU]"J2$PH7R*#(&lt;9W&lt;.\\NF\\LWT,G0M\LF3=40$W#(/W'GWS6K06N@9R.L,M!085LVR0@I6;#HO+#F](9B6&amp;1CZR&lt;I=/4^:ZOIGUC_%L#G];:=D7#C&amp;PB4(U:!BWA:"!-,0P?&amp;QK,\%AWT=2TKZ$_K"]+-_G-`H#%#"G*#^*6E6S$[J%5B&gt;"8X:UEOW;NBKXF)VQ,L]J'+&lt;%F%1^7SD?W,(A`9*85SO!&amp;:&gt;VK@5.?(O(M738'IOP[/N_+P[VA9^KS`-`GU8EU#YK#QA&amp;B!JCY6KS)Q;90M.R,VM&lt;'&amp;&gt;:%9ZWI@VE?A$G:1U7K".,NZZO9DB=EBL*HPGN:GH:KC:;P!^-YH+59_[+W-PJ8'OX+62MF?@D2@VO1B)_=E#A&gt;181@FATM+H&lt;_C)Z%ACNVTI6N)9(]@Y+-;\&amp;@\`'*UCRMR-98_GK6_)BO%W:W(U*R.GP?_B7I$]10W/0A7X&lt;K]U0U\^0_^W@WT4M@-*DG:`$RE;+OXH+5`0.V,B1YTTF,O;RN_QH/&amp;I(UNVSR/[EXS=H^&lt;=P+R/`_=E+?"O07Y^GVHP?-Z+XOFY4;@OL(=#RVQ&lt;R.7&lt;ZM6T0/&gt;J[+QVSG@0&lt;`Q$4JP=,1!!!!!/&amp;Q'!%!!!"D%X,D!O-1!!!!!!!!Q8!)!!!!!%-4=O-!!!!!!/&amp;Q'!%!!!"D%X,D!O-1!!!!!!!!Q8!)!!!!!%-4=O-!!!!!!/&amp;Q'!%!!!"D%X,D!O-1!!!!!!!"1"!!!!^6=VAHEGIIQO=V*/"EEZH1!!!!U!!!!!!!!!!!!!!!!!!!!!!!!!A0````_!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9"A!!'"G!!"BA9!!:A"A!'A!%!"M!$!!;Q$1!'D$M!"I06!!;!KQ!'A.5!"I#L!!;!V1!'A+M!"I$6!!:ALA!''.A!"A&lt;A!!9"A!!(`````!!!%!0```````````````````````````````````````````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!(BY!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!(CMKKOM?!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!(CMKK/DI[/LL(A!!!!!!!!!!!!!!!!!!!!!``]!!(CMKK/DI[/DI[/DK[RY!!!!!!!!!!!!!!!!!!$``Q#LKK/DI[/DI[/DI[/DI[OM!!!!!!!!!!!!!!!!!0``!+KKI[/DI[/DI[/DI[/D`KM!!!!!!!!!!!!!!!!!``]!KKOLKK/DI[/DI[/D`P\_KA!!!!!!!!!!!!!!!!$``Q#KK[OLK[KDI[/D`P\_`P[K!!!!!!!!!!!!!!!!!0``!+KLK[OLK[OKL0\_`P\_`KI!!!!!!!!!!!!!!!!!``]!KKOLK[OLK[P_`P\_`P\_KA!!!!!!!!!!!!!!!!$``Q#KK[OLK[OLK`\_`P\_`P[K!!!!!!!!!!!!!!!!!0``!+KLK[OLK[OL`P\_`P\_`KI!!!!!!!!!!!!!!!!!``]!KKOLK[OLK[P_`P\_`P\_KA!!!!!!!!!!!!!!!!$``Q#KK[OLK[OLK`\_`P\_`P[K!!!!!!!!!!!!!!!!!0``!+OLK[OLK[OL`P\_`P\_K[M!!!!!!!!!!!!!!!!!``]!!+3KK[OLK[P_`P\_K[SE!!!!!!!!!!!!!!!!!!$``Q!!!!#EK[OLK`\_K[OE!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!J+OLK[OD!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!+3D!!!!!!!!!!!!!!!!!!!!!!!!!!$```````````````````````````````````````````]!!!(@!!&amp;'5%B1!!!!!Q!#6%2$1Q!!!!%:1W^O:GFH&gt;8*B&gt;'FP&lt;CV$&lt;(6T&gt;'6S,G.U&lt;&amp;"53$!!!!!I!!%!"!!!"U.P&lt;H2S&lt;WQ:1W^O:GFH&gt;8*B&gt;'FP&lt;CV$&lt;(6T&gt;'6S,G.U&lt;!!!!!)!!0]!!!!"!!%!!!!!!!M!!!!!!!!!!!!!!!!!!!!!!!!!!1!!!Q&gt;16%AQ!!!!!!!!!!!!!F2%1U-!!!!!!B&amp;/36^-6E.P&lt;G:J:SZM&gt;GRJ9B:$&lt;WZG;7=A2'&amp;U93"3:7:/&gt;7UO9X2M!&amp;"53$!!!!!W!!!!"!=]&gt;GFM;7)_"V6U;7RJ&gt;(E+9W^O:GFH,GRM9B:$&lt;WZG;7=A2'&amp;U93"3:7:/&gt;7UO9X2M!!!!!!!!1A!!!!!!!!!"!!!!IV"53$!!!!!R!!!!"!=]&gt;GFM;7)_"V6U;7RJ&gt;(E+9W^O:GFH,GRM9B&amp;/36^-6E.P&lt;G:J:SZM&gt;GRJ9A!#6%2$1Q!!!!!!!2&gt;198.T&gt;W^S:#"-:8:F&lt;#V&amp;&lt;H6N,G.U&lt;&amp;"53$!!!!!G!!%!"!!!"U.P&lt;H2S&lt;WQ85'&amp;T=X&gt;P=G1A4'6W:7QN27ZV&lt;3ZD&gt;'Q!!!!#!!$`!!!!!1!"!!!!!!!,!!!!!!!!!!!!!!!!!!!!!!!!!!%!!!)P5&amp;2)-!!!!!!!!!!!!!-!!!!,NA!!-&gt;.YH-V&lt;$8"56R5_\\V.MJP&gt;*'_4&lt;**F;,**8S*1!O((B+;#%"9&gt;@AQ`7Q&lt;N&gt;*!UO[%:]A/\31D#5+:OUY:/L&lt;7+A^)JWBH]L4C#VCJKK1&amp;L&amp;[6&amp;2[@1.J3JH6&amp;'R:^9BP:F0@?_`Z`.\CQ**"EOD]U^ZZZXTH@/0@?\!7$"?&lt;[#(9@^)D$]'$[M%S%`H'!!2OK&gt;)(`6$1'`G@E!G&amp;)`)]*SZW&lt;_%DP/T"4"%U\5/2O#QX!.:S@0*`?S/^DF`$MY.:@XI\*]%9L#C8,P'G'5&amp;Y\0&amp;):T&amp;+V?K/+@9-&lt;:4QN6\TO@C0@CAB#@25:P04-/4,$;Y9D8&lt;'DLC=1&amp;]KGLXOGH+FUC]-&amp;%1619`1BKR+6@JCL:E_TD\"F&amp;*;$+78$CR!F.S#M*V6%TFK!-_TB+(7&amp;04C"4(%T=%2.'ZV+:@#K$[RR2VAF7D4^.&lt;#&gt;#:N'39)*(5:1&lt;F&amp;_:S#JSYH)K&gt;_8+&amp;:4$5::\5)239&lt;4&amp;[8?_%\\)&amp;'S-PQ!--0(85$JPZ,0/Z.:EC"VDH`?WEFB1C7)-2WUTQY@Q/34#H(C#X19/:=51@)M'Q[%%YW-9$/94."COY$!07D3/J)F'U$&amp;3=]@+LPZ98S1;[/U)N(?VR7+"H&gt;(/A&lt;;_3#$=VN&gt;GD&gt;039-,62(R!&amp;K-1!2]YW08QH.\HP8$MW$&amp;U!Y[;[$)5,2&gt;'64GP`$;+X]/;X]GKGP]_DPY,8L^X/`&amp;B&lt;4/L)$?@)P&gt;&gt;/O[AYT!:O4T[@)+-D*M_([8D._HY("H:_2L?&amp;S,?P&lt;TZ:1RY`_DEY\U29&lt;D@B(&gt;YFGW'(U[!X3:*3)@X:P4B7HBW!JEF+0/1!?`.O-Z;::W*]([X&amp;?`.SFIKXA]&gt;/G31QXAVKXBX-)S#^_3(S1]*XA?3&lt;\(HY&amp;7+^XQK%=:Q#)E!,I&amp;/!X9WB+HX/&gt;H\\!)&amp;Y-(D67MUB/?C`\&gt;/\0^!,C$+B:';QAW)\.W^U8"A871AUG5+RR;28220,(=!"%?LVB$NR@DDOW1\]B1\+!J;$9G7`!@E*J.1F"9'1AW9&amp;LUMMIXBB-/0M-L8:7YO&gt;%!Z$#B/4HKJES^=O)"/PH(DBK1R8AU(($6RA8TCKI`XV=ZL[*$6&amp;IAMIM3V5"DV[&amp;,5A&gt;]_W*;"6E(2/D+0T[0/#CS5`VYA,_%27136@ZEQSN)F'KAHEMER=+/38DBV[B3KR&gt;&amp;&lt;B?ZD?4]TTA6'KK3=]9!H(K"4KMH)V]AA&lt;(@7%087K12@V71U4H66;&gt;9A4PW)/5[T"AL1'H`WVDS1O450'+RRC7STK=S2,T^CS6DGK/].:9[^"^/G^D.1?R^AM9N)R9[L'W,L!F"8H;SL!?^6@!.Q8&gt;5S\&gt;M[E9+VEIA$[_.\4]JTQFDQZ.Q[$9"2HG0),@CF@7YRG&amp;NTUO17#^)/ELOSN[?D=\M*XGD=':.R^00.)PR7S&lt;)3&amp;&amp;AK7[2O:[`&lt;:.H@A5HC$\0)-KRC&amp;T!?*;:YT&amp;(?$H/"4V8_`G$&gt;[H,QOR:K48H5=@A=SO+IP@M@V8?8AJ*$.KU1L48Q:SSLI.?GLT,QBMX\`T8Z1@)'V'5;E'CEI[?`W_K*CZAH:E]%)+#I(;/&lt;"S;S7ELA%EIU'EO*3?B`V!0*=RV%^&amp;S(U2;M*@A:K35?&gt;TN&amp;#?UK:0VO%&gt;[5^%NVJ')ZU?]"&amp;V2!I;R/T6HK\JV35N,H,FX7I;;X*%V3$3A^1$1ZA=&gt;37G;LK6?H;9&gt;/UW52XC:6O&gt;69F;8&gt;XP43+G45@AL22`YNF_3Q"I&gt;2%RRS&lt;&gt;-[*0UQ$\(S3/G,RQ\&amp;V42?D^U,S?($G*KH99ER9XZ%=PC4&amp;$%_I]&amp;,-NE?%4&amp;/:8MU9=9DQEF-!OR@@6J`R%)&gt;F/$L6[Z&amp;&lt;73ELH59K[58P&amp;ANS:2K-KL6MMN;7+7JY33:CG0[K4?P6&lt;&gt;ZQ!N3FG_BTCM#J3\.!J];9NS3\&gt;\2BV-#&gt;%IVG;)O%&lt;&amp;;9ZC;T(TKY?SHKO_)]0O&amp;LB*P+J61ZE35$7UF@J,H&lt;=!_/:T)B29#N&amp;Y%WG')'5P4&gt;WR0'G\5%=M1;,T;B[U)B[/27-T3&amp;-&amp;X-@V)_Z*P/'%5)?C7GMIONCG7^I7V&lt;9IQT.`$-$=;/B;M$A!T=6?`^2U,@&amp;^KU@1&gt;3S8&gt;FL+W*OO/"7PH]V,`6#3&amp;^38.YS1&amp;XLDY*FL40X357J/D6-XL$6I&amp;P4Z@^TR0^VSP0&lt;``OK&amp;,AB^9$I.O@0U6[1_$R_FB=*.&lt;1L&amp;,8UB6)$.O&amp;=CL%-C^M-M!:':83C$PSH#0Z6@XB#/$!7R^?C,N@:W^029A-^%J!4)4GUZ!:PKG%Z#:`FM0:'9A3S!TO`6&amp;79:TPAW=.YFQAC$:CUB?B=&gt;PAG385J*`&lt;)PE27D&amp;PAR+MA/2\.9Q&lt;+\'O#0]**TQO+,9M,=)RSO&amp;Y=5+X/&lt;#)GJ)DF+W@UL1(D7C01`.K)$Z.V/W8^4D+]!1L955\98J]&amp;7)5T,%FT1V(&lt;\1GJ^*VOBT4Z(0UBK&lt;X%NF4&lt;PJI!E`2WNW[Z$(11%[:F([%HJ+@^$MF*$H2O2JKH_&amp;KL=)IZ8YID,@29)_$RK.**`.I?EFF)Q:$UXFU!1.](54$'Q)QN-K*_:3/,&amp;S/+D*3FAW,@CSSIGZ*%Y-KP4LW=L]7O8%8")HBD+'&gt;=:3P&gt;Y)PNZGH=^,-.=7;ZR9;J^`1_`T"S7@?_K'/(IIX#D#+]KB50'7A9"]V@:1$(AI&amp;D,A?(.$@&gt;(/(P-*(4@_B+&amp;?/K5TK1/T.N^3,RVK`8N&amp;6QN`IXM_KXM_9WQQTEHL$/DK-EM:+&lt;)/LD!J[[#&lt;@I@L9([[UT*@&gt;D8)I&gt;1A,&gt;K`2XXJK2S&lt;;*`82`N-\6Z7#HA""NQ:KDM!\TV6\#.]P_#(@&amp;^RG@*96FSO0*&lt;,SB!;QW4J&gt;;B`5'9E66B]+38V8Z^B(Z-8MGV@-':07\&amp;2A&lt;Y5*BM&lt;8\:CQY@LT*JM&lt;(R&amp;KNVO1W%C[W3*D5/G@&lt;_%\II:9//L^NAI*.B13$MO4S,N-/3!'X_`A&lt;4D80;E(9H\RATD8C#2&gt;PX2.JPIBU4/,6U[[:G[Q\):#O`!&amp;;1M3K[MG$KOU%KYE@2^7/P+5^UN=55WJ/M1'JQO'BSPDY:]QV3EX$"R^$[JEI[T[&lt;B9P4@C3ATX2IKB_PTE@*._&lt;]36W&gt;U&lt;&gt;=._W*`[$IAL.^]&lt;Y8S!*[&amp;\!JE+]\U2SH3DT0[U^U;=XX*P2'44XBNR-QQMG?XNU;LE8`#$R_DN%3=XK&gt;RCZ&lt;380)HTXI:/QWG0;S22M$XN&gt;7&gt;)7XC$E;[W09%6(?3C&gt;%OUM]]=G-MCVT1FRTVOC:YBP&gt;X(0?ZO;9/&gt;(M=^LPH7(`?Y?UQN1=&lt;(P3;&lt;YRZPSVZQMV5](U1]HT4D_;[M];SS&amp;XI]&lt;YKUG8F@B00=K9&amp;T`&lt;3#]\RJ"?@ZNQ(/$&gt;H#_5Y&lt;/(PNY6SJQHE&lt;QPEA,$0#/:!3TCW:8F`=W^E&gt;#;TP\\0#O(JK9&amp;QTL7"]Z\3#M8!&lt;9&amp;S&lt;*9TBOAW-CYUQ&gt;L8KOC4HJ&lt;0^TG1-=9R`.$60W&gt;T$F_DPY4?)]#Z*!Z;EQ4`J-@*_Y_8+XV*SUP&gt;HWN/(ONI')KFP6K\;ZI)P@3[1*7TS96!DRKZ.*V);`D7&gt;3'HYNW2.GHQ1PMB-\A8,@\+^90GP45K5WP03/YBC0UKPAXV'8LI\*3`&gt;GA';S@'H:9_FS]9E[L(BIQNJ&lt;%V]^-[JY;.X43M_/DKN_/B9NHRUHQU@\40QU&lt;MN@(1BJ6P3]^'$6D\;"UUQ(Q&lt;4]^&amp;\L(SU$\:LML&lt;=]O=M@(3F@DV&lt;G&lt;U70LL3O%Z+0HK@C9`W9IYN5OZ_*P,ZJWTY[$+&amp;D]:%/Y#\&amp;&gt;_C&gt;Z&gt;BNXL9BPOZ2H\`$\O\$*H(VP\O3,3TX&lt;J0@:\]!IZJH]L"CFFW=\]"%*?[*(/S?'\0(P7)N7=DE*ZR?`;I)7P0*HF]#H]$Y&amp;&amp;4T]9A@':EME%^:L."F2MWK)QJ&gt;GNP"`+8-S1%EW@PKTU+%XTN63:0I)B@B69`*))PO,,Y0,];&amp;`=Y6TN8]=_QYQQD1JHT'?@9S*`5`WARME[6D,]'@?Q8B+^6/3P_$W4G^UU!!!!!!!1!!!$9!!!!"!!!!!!!!!!-!!&amp;#2%B1!!!!!!!$!!!!9A!!!(*YH'.A9-A4E'$[RV$XFY&amp;*Y#O1)@W8A6H1D`%X!Q/HH]"B)-UI)!E5FPX,Q#[I$2&lt;70K,,Q1!&amp;KGS-(*)=BQ5ZQ$)=,2I-````Z`B[Z"J=R2%@/&amp;.FFDS("!!59BE!!!!!!!!%!!!!"Q!!#-U!!!!(!!!!)6^O;6^-98.U3WZP&gt;WZ0&gt;WZJ&lt;G&gt;-6E.M98.T1WRV=X2F=A!!!FI8!)!!!!!!!1!)!$$`````!!%!!!!!!DY!!!!2!".!"1!.5WRB&gt;G5A172E=G6T=Q!.1!5!"UZV&lt;76S;7-!%E"!!!(`````!!%%1HFU:1!!%%!Q`````Q&gt;4:7.U;7^O!!^!"Q!)6'FN:3"0&gt;81!!"&gt;!"Q!12'6M98EA17:U:8)A5G6B:!!!&amp;U!(!"&amp;%:7RB?3"":H2F=C"8=GFU:1"?!0%!!!!!!!!!!QV65U)N34*$,GRW&lt;'FC$V641CV*-E-O&lt;(:D&lt;'&amp;T=RF$&lt;WZG;7&gt;V=G&amp;U;7^O,5.M&gt;8.U:8)O9X2M!"Z!5!!$!!1!"1!'$5.P&lt;G:J:X6S982J&lt;WY!%%!Q`````Q:4&gt;(*J&lt;G=!!"B!1!!"`````Q!)#U.P&lt;GZF9X2J&lt;WZT!"&gt;!"Q!137ZE:8AA1W^O&lt;G6D&gt;'FP&lt;A!!&amp;U!&amp;!""198.T&gt;W^S:#"":'2S:8.T!!!/1#%)5'&amp;T=X&gt;P=G1!!"N!&amp;A!"#W.P&lt;G:J:S"E982B!!:S:7:O&gt;7U!!%=!]&lt;GGFI1!!!!#%5Z*8UR71W^O:GFH,GRW&lt;'FC&amp;E.P&lt;G:J:S"%982B)&amp;*F:EZV&lt;3ZD&gt;'Q!&amp;E"Q!!%!!1!."E.P&lt;G:J:Q!!;1$R!!!!!!!!!!-.66.#,5ES1SZM&gt;GRJ9A^65U)N34*$,GRW9WRB=X-85'&amp;T=X&gt;P=G1A4'6W:7QN27ZV&lt;3ZD&gt;'Q!+U!7!!)(4'6W:7QA-A&gt;-:8:F&lt;#!R!!Z198.T&gt;W^S:#"-:8:F&lt;!!!+E"1!!I!!!!#!!-!"Q!*!!I!#Q!-!!Y!$Q^65U)N34*$,GRW9WRB=X-!!1!1!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;597*0=G2F=A!!!&amp;E8!)!!!!!!!A!&amp;!!=!!!Q!1!!"`````Q!!!!%!!1!!!!U!!!!!!!!!!1!!!!)!!!!$!!!!"!!!!!5!!!!'!!!!"Q!!!!A!!!!*!!!!#A!!!!M!!!!-!!!!!!!!!"N-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5;7VF=X2B&lt;8!!!!!:&amp;Q#!!!!!!!%!"1!(!!!"!!$8ZB+@!!!!!!!!!#:-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;-98.U18"Q&lt;'FF:&amp;2J&lt;76T&gt;'&amp;N=!!!!"E8!)!!!!!!!1!&amp;!!=!!!%!!.@G%J]!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962Z='6%:8.D!!!#7B=!A!!!!!!"!!A!-0````]!!1!!!!!#0A!!!"%!%U!&amp;!!V4&lt;'&amp;W:3"":'2S:8.T!!V!"1!(4H6N:8*J9Q!31%!!!@````]!!12#?82F!!!11$$`````"V.F9X2J&lt;WY!$U!(!!B5;7VF)%^V&gt;!!!&amp;U!(!""%:7RB?3"":H2F=C"3:7&amp;E!!!81!=!%52F&lt;'&amp;Z)%&amp;G&gt;'6S)&amp;&gt;S;82F!&amp;Y!]1!!!!!!!!!$$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T'5.P&lt;G:J:X6S982J&lt;WYN1WRV=X2F=CZD&gt;'Q!(E"1!!-!"!!&amp;!!9.1W^O:GFH&gt;8*B&gt;'FP&lt;A!11$$`````"F.U=GFO:Q!!'%"!!!(`````!!A,1W^O&lt;G6D&gt;'FP&lt;H-!&amp;U!(!""*&lt;G2F?#"$&lt;WZO:7.U;7^O!!!81!5!%&amp;"B=X.X&lt;X*E)%&amp;E:(*F=X-!!!Z!)1B198.T&gt;W^S:!!!'U!7!!%,9W^O:GFH)'2B&gt;'%!"H*F:GZV&lt;1!!2Q$RO;;7B!!!!!)24EF@4&amp;:$&lt;WZG;7=O&lt;(:M;7)71W^O:GFH)%2B&gt;'%A5G6G4H6N,G.U&lt;!!71(!!!1!"!!U'1W^O:GFH!!"J!0%!!!!!!!!!!QV65U)N34*$,GRW&lt;'FC$V641CV*-E-O&lt;(:D&lt;'&amp;T=R&gt;198.T&gt;W^S:#"-:8:F&lt;#V&amp;&lt;H6N,G.U&lt;!!L1"9!!A&gt;-:8:F&lt;#!S"URF&gt;G6M)$%!$F"B=X.X&lt;X*E)%RF&gt;G6M!!!K1&amp;!!#A!!!!)!!Q!(!!E!#A!,!!Q!$A!0$V641CV*-E-O&lt;(:D&lt;'&amp;T=Q!"!"!!!!!!!!!!(ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B5WF[:1!!!"E8!)!!!!!!!1!&amp;!!-!!!%!!!!!!#5!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B!!!#;R=!A!!!!!!2!".!"1!.5WRB&gt;G5A172E=G6T=Q!.1!5!"UZV&lt;76S;7-!%E"!!!(`````!!%%1HFU:1!!%%!Q`````Q&gt;4:7.U;7^O!!^!"Q!)6'FN:3"0&gt;81!!"&gt;!"Q!12'6M98EA17:U:8)A5G6B:!!!&amp;U!(!"&amp;%:7RB?3"":H2F=C"8=GFU:1"?!0%!!!!!!!!!!QV65U)N34*$,GRW&lt;'FC$V641CV*-E-O&lt;(:D&lt;'&amp;T=RF$&lt;WZG;7&gt;V=G&amp;U;7^O,5.M&gt;8.U:8)O9X2M!"Z!5!!$!!1!"1!'$5.P&lt;G:J:X6S982J&lt;WY!%%!Q`````Q:4&gt;(*J&lt;G=!!"B!1!!"`````Q!)#U.P&lt;GZF9X2J&lt;WZT!"&gt;!"Q!137ZE:8AA1W^O&lt;G6D&gt;'FP&lt;A!!&amp;U!&amp;!""198.T&gt;W^S:#"":'2S:8.T!!!/1#%)5'&amp;T=X&gt;P=G1!!"N!&amp;A!"#W.P&lt;G:J:S"E982B!!:S:7:O&gt;7U!!%=!]&lt;GGFI1!!!!#%5Z*8UR71W^O:GFH,GRW&lt;'FC&amp;E.P&lt;G:J:S"%982B)&amp;*F:EZV&lt;3ZD&gt;'Q!&amp;E"Q!!%!!1!."E.P&lt;G:J:Q!!;1$R!!!!!!!!!!-.66.#,5ES1SZM&gt;GRJ9A^65U)N34*$,GRW9WRB=X-85'&amp;T=X&gt;P=G1A4'6W:7QN27ZV&lt;3ZD&gt;'Q!+U!7!!)(4'6W:7QA-A&gt;-:8:F&lt;#!R!!Z198.T&gt;W^S:#"-:8:F&lt;!!!+E"1!!I!!!!#!!-!"Q!*!!I!#Q!-!!Y!$Q^65U)N34*$,GRW9WRB=X-!!1!1I!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"\!!!!!!!!!!!!!!!!!!!!!!!%!"=!&amp;A!!!!1!!!-\!!!!+!!!!!)!!!1!!!!!%A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!+2!!!'/(C=L64*&lt;N.1&amp;$W/US1F*%V&gt;3$LTSF3A*(2AQ1)*&gt;Q"55:7I9&gt;A"*H[O,,F/Z4CBX&lt;(A'^CSYR`Y%P[!.@E#/"[3.#KIJ=68=83P\X$?O=='="]D?BK:&amp;`;?&amp;-^&lt;0F#C7^C1DH%I6CV@?G*('G95(DU;@OX:PM1&lt;&gt;""&gt;;OZF&lt;;W]O&lt;R?=&gt;K/`8[E\^5&gt;I^G=7'_YFLX&lt;]AT@&lt;LDF&gt;;@6:*&gt;+X8=QKV?BMI/#2'YA#V.[%5KW(M;%;@A'5J[UX.9?]!S&gt;&lt;V]`@W*:9H2\]_X7K[AS'F[-(,("%O+XNFN\Y;2*@:^4&amp;#1(?NKH05/JSNO(BG?+,&gt;G74PGR'`&gt;&gt;)-Z%/AS+Z@B`#@H"&gt;'"-(U+OZBBN+6:.UZ0.*H)-J1F0?H9&gt;GKZ$_=5,G?4;)&gt;F&amp;16]-`(2.VE.#9D^6]TX&lt;X18'?R8:,-`M2GH.;)G&lt;LCE02$]=,(%)B2[K,A&lt;E^&lt;F-.QI]01OTR3[TK3A"=.(Z`E0\=D+LW;Y@^(F),6R!'M/]K\C)(0)911'JG6ARIG'*M%TM?X&lt;&lt;]'7UQS%;3QB@_1G.DBK\7-%F$*-/V8*WE&gt;%`KIU&gt;0QL)BE6#!_U65=+YWD)N4OQ3KC'$E'QW3Q06`S$U!I*BU[(V"UUBC4E/31'0TE*^(B(V)ND=/95](+#9$8?A9:,1:D",M"KO%/*6V4]Q_;3+"#:QD7`4X^Z&lt;\:A;ZE`XBDQYMPY&lt;P@8@R0S*[U_!.Q6=WDN#3O)*91L=ZE],\1ZFU$5NNO/2`J/&amp;U#)*+&lt;A\A/R@B*F%_+W+^[+=^R.3QJ`3C#EGH-B[3S&amp;K.&gt;46+-:Q'&gt;&gt;R#W65='`A&amp;-%:&amp;BGLU*;9O]Q?"5&lt;T@";I.-P-&amp;@:3!F7R=JKVG&gt;^&lt;QX0D!!!!!!!!D!!"!!)!!Q!%!!!!3!!0!!!!!!!0!/U!YQ!!!&amp;Y!$Q!!!!!!$Q$N!/-!!!"U!!]!!!!!!!]!\1$D!!!!CI!!A!#!!!!0!/U!YR6.;7.S&lt;X.P:H1A3GBF&lt;G&gt;):7EA65E647FD=G^T&lt;W:U)%JI:7ZH3'6J)&amp;6*&amp;5VJ9X*P=W^G&gt;#"+;'6O:UBF;3"631%Q5F.31QU+!!.-6E.$4%*76Q!!+2Q!!!21!!!!)!!!+0Q!!!!!!!!!!!!!!#!!!!!U!!!%2!!!!"N-35*/!!!!!!!!!62-6F.3!!!!!!!!!7B36&amp;.(!!!!!!!!!8R$1V.5!!!!!!!!!:"-38:J!!!!!!!!!;2$4UZ1!!!!!!!!!&lt;B544AQ!!!!!1!!!=R%2E24!!!!!!!!!@2-372T!!!!!!!!!AB735.%!!!!!A!!!BRW:8*T!!!!"!!!!FB41V.3!!!!!!!!!LR(1V"3!!!!!!!!!N"*1U^/!!!!!!!!!O2J9WQY!!!!!!!!!PB-37:Q!!!!!!!!!QR'5%BC!!!!!!!!!S"'5&amp;.&amp;!!!!!!!!!T275%21!!!!!!!!!UB-37*E!!!!!!!!!VR#2%BC!!!!!!!!!X"#2&amp;.&amp;!!!!!!!!!Y273624!!!!!!!!!ZB%6%B1!!!!!!!!![R.65F%!!!!!!!!!]")36.5!!!!!!!!!^271V21!!!!!!!!!_B'6%&amp;#!!!!!!!!!`Q!!!!!`````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!0````]!!!!!!!!!T!!!!!!!!!!!`````Q!!!!!!!!$A!!!!!!!!!!$`````!!!!!!!!!/A!!!!!!!!!!0````]!!!!!!!!#A!!!!!!!!!!!`````Q!!!!!!!!+)!!!!!!!!!!,`````!!!!!!!!!L!!!!!!!!!!!0````]!!!!!!!!#S!!!!!!!!!!!`````Q!!!!!!!!-=!!!!!!!!!!$`````!!!!!!!!!SQ!!!!!!!!!!@````]!!!!!!!!%\!!!!!!!!!!#`````Q!!!!!!!!&gt;Q!!!!!!!!!!4`````!!!!!!!!#91!!!!!!!!!"`````]!!!!!!!!*G!!!!!!!!!!)`````Q!!!!!!!!GI!!!!!!!!!!H`````!!!!!!!!#&lt;Q!!!!!!!!!#P````]!!!!!!!!*T!!!!!!!!!!!`````Q!!!!!!!!HA!!!!!!!!!!$`````!!!!!!!!#@A!!!!!!!!!!0````]!!!!!!!!+$!!!!!!!!!!!`````Q!!!!!!!!K1!!!!!!!!!!$`````!!!!!!!!$J1!!!!!!!!!!0````]!!!!!!!!1?!!!!!!!!!!!`````Q!!!!!!!"QU!!!!!!!!!!$`````!!!!!!!!($Q!!!!!!!!!!0````]!!!!!!!!=2!!!!!!!!!!!`````Q!!!!!!!"R5!!!!!!!!!!$`````!!!!!!!!(,Q!!!!!!!!!!0````]!!!!!!!!=R!!!!!!!!!!!`````Q!!!!!!!#79!!!!!!!!!!$`````!!!!!!!!*;!!!!!!!!!!!0````]!!!!!!!!FK!!!!!!!!!!!`````Q!!!!!!!#85!!!!!!!!!)$`````!!!!!!!!+'Q!!!!!#V641CV*-E-O9X2M!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!AV65U)N34*$,GRW&lt;'FC$V641CV*-E-O&lt;(:D&lt;'&amp;T=V"53$!!!!!!!!!!!!!!!!!!$1!"!!!!!!!!!1!!!!%!&amp;E"1!!!066.#,5ES1SZM&gt;G.M98.T!!%!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!!!!!!!!!%!!!!!!!!!!!!!!1!71&amp;!!!!^65U)N34*$,GRW9WRB=X-!!1!!!!!!!@````Y!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!!!!!!!!!%!!!!!!!%!!!!!"!!.1!5!"UZV&lt;76S;7-!%E"!!!(`````!!!%1HFU:1!!%U!&amp;!!V4&lt;'&amp;W:3"":'2S:8.T!&amp;Y!]&gt;=#[@I!!!!$$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T#V641CV*-E-O9X2M!#R!5!!#!!%!!BV$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!-!!!!#``````````]!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!!!!!!!!!%!!!!!!!)!!!!!#1!.1!5!"UZV&lt;76S;7-!%E"!!!(`````!!!%1HFU:1!!%U!&amp;!!V4&lt;'&amp;W:3"":'2S:8.T!"N!&amp;A!"#W.P&lt;G:J:S"E982B!!:S:7:O&gt;7U!!%U!]&lt;GGFI1!!!!#%5Z*8UR71W^O:GFH,GRW&lt;'FC&amp;E.P&lt;G:J:S"%982B)&amp;*F:EZV&lt;3ZD&gt;'Q!(%"Q!!%!!1!$$5.P&lt;G:J:X6S982J&lt;WY!$U!(!!B5;7VF)%^V&gt;!!!&amp;U!(!""%:7RB?3"":H2F=C"3:7&amp;E!!!81!=!%52F&lt;'&amp;Z)%&amp;G&gt;'6S)&amp;&gt;S;82F!'9!]&gt;=#[^I!!!!$$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T#V641CV*-E-O9X2M!$2!5!!'!!%!!A!%!!5!"A!((5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!#!!!!!9!!!!!!!!!!@````````````````````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!!!!!!!!!%!!!!!!!-!!!!!#A!.1!5!"UZV&lt;76S;7-!%E"!!!(`````!!!%1HFU:1!!%U!&amp;!!V4&lt;'&amp;W:3"":'2S:8.T!"N!&amp;A!"#W.P&lt;G:J:S"E982B!!:S:7:O&gt;7U!!%=!]&lt;GGFI1!!!!#%5Z*8UR71W^O:GFH,GRW&lt;'FC&amp;E.P&lt;G:J:S"%982B)&amp;*F:EZV&lt;3ZD&gt;'Q!&amp;E"Q!!%!!1!$"E.P&lt;G:J:Q!!$U!(!!B5;7VF)%^V&gt;!!!&amp;U!(!""%:7RB?3"":H2F=C"3:7&amp;E!!!81!=!%52F&lt;'&amp;Z)%&amp;G&gt;'6S)&amp;&gt;S;82F!&amp;Y!]1!!!!!!!!!$$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T'5.P&lt;G:J:X6S982J&lt;WYN1WRV=X2F=CZD&gt;'Q!(E"1!!-!"1!'!!=.1W^O:GFH&gt;8*B&gt;'FP&lt;A"C!0(8!PU[!!!!!QV65U)N34*$,GRW&lt;'FC$V641CV*-E-O&lt;(:D&lt;'&amp;T=QN65U)N34*$,G.U&lt;!!Q1&amp;!!"!!"!!)!"!!)(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!#1!!!!1!!!!!!!!!!1!!!!,`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"=!A!!!!!!!!!!!!!!!!!!"!!!!!!!%!!!!!!M!$5!&amp;!!&gt;/&gt;7VF=GFD!"*!1!!"`````Q!!"%*Z&gt;'5!!".!"1!.5WRB&gt;G5A172E=G6T=Q!&lt;1"9!!1ND&lt;WZG;7=A:'&amp;U91!'=G6G&lt;H6N!!"(!0'ZJJ;%!!!!!B&amp;/36^-6E.P&lt;G:J:SZM&gt;GRJ9B:$&lt;WZG;7=A2'&amp;U93"3:7:/&gt;7UO9X2M!":!=!!"!!%!!Q:$&lt;WZG;7=!!!^!"Q!)6'FN:3"0&gt;81!!"&gt;!"Q!12'6M98EA17:U:8)A5G6B:!!!&amp;U!(!"&amp;%:7RB?3"":H2F=C"8=GFU:1"?!0%!!!!!!!!!!QV65U)N34*$,GRW&lt;'FC$V641CV*-E-O&lt;(:D&lt;'&amp;T=RF$&lt;WZG;7&gt;V=G&amp;U;7^O,5.M&gt;8.U:8)O9X2M!"Z!5!!$!!5!"A!($5.P&lt;G:J:X6S982J&lt;WY!%%!Q`````Q&gt;4:7.U;7^O!'1!]&gt;=$!SQ!!!!$$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T#V641CV*-E-O9X2M!$*!5!!&amp;!!%!!A!%!!A!#2V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!I!!!!)!!!!!!!!!!%!!!!#!!!!!Q!!!!1!!!!&amp;!!!!"P````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!!!!!!!!!%!!!!!!!5!!!!!#Q!41!5!$6.M98:F)%&amp;E:(*F=X-!$5!&amp;!!&gt;/&gt;7VF=GFD!"*!1!!"`````Q!""%*Z&gt;'5!!"N!&amp;A!"#W.P&lt;G:J:S"E982B!!:S:7:O&gt;7U!!%=!]&lt;GGFI1!!!!#%5Z*8UR71W^O:GFH,GRW&lt;'FC&amp;E.P&lt;G:J:S"%982B)&amp;*F:EZV&lt;3ZD&gt;'Q!&amp;E"Q!!%!!1!$"E.P&lt;G:J:Q!!%%!Q`````Q&gt;4:7.U;7^O!!^!"Q!)6'FN:3"0&gt;81!!"&gt;!"Q!12'6M98EA17:U:8)A5G6B:!!!&amp;U!(!"&amp;%:7RB?3"":H2F=C"8=GFU:1"?!0%!!!!!!!!!!QV65U)N34*$,GRW&lt;'FC$V641CV*-E-O&lt;(:D&lt;'&amp;T=RF$&lt;WZG;7&gt;V=G&amp;U;7^O,5.M&gt;8.U:8)O9X2M!"Z!5!!$!!9!"Q!)$5.P&lt;G:J:X6S982J&lt;WY!:!$RVQ-$=!!!!!-.66.#,5ES1SZM&gt;GRJ9A^65U)N34*$,GRW9WRB=X-,66.#,5ES1SZD&gt;'Q!-E"1!!5!!!!#!!1!"1!*(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!#A!!!!A!!!!"!!!!!!!!!!)!!!!(!!!!!Q!!!!1!!!!&amp;!!!!"A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!8!)!!!!!!!!!!!!!!!!!!!1!!!!!!"A!!!!!/!".!"1!.5WRB&gt;G5A172E=G6T=Q!.1!5!"UZV&lt;76S;7-!%E"!!!(`````!!%%1HFU:1!!'U!7!!%,9W^O:GFH)'2B&gt;'%!"H*F:GZV&lt;1!!2Q$RO;;7B!!!!!)24EF@4&amp;:$&lt;WZG;7=O&lt;(:M;7)71W^O:GFH)%2B&gt;'%A5G6G4H6N,G.U&lt;!!71(!!!1!"!!-'1W^O:GFH!!!11$$`````"V.F9X2J&lt;WY!$U!(!!B5;7VF)%^V&gt;!!!&amp;U!(!""%:7RB?3"":H2F=C"3:7&amp;E!!!81!=!%52F&lt;'&amp;Z)%&amp;G&gt;'6S)&amp;&gt;S;82F!&amp;Y!]1!!!!!!!!!$$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T'5.P&lt;G:J:X6S982J&lt;WYN1WRV=X2F=CZD&gt;'Q!(E"1!!-!"A!(!!A.1W^O:GFH&gt;8*B&gt;'FP&lt;A!11$$`````"F.U=GFO:Q!!'%"!!!(`````!!I,1W^O&lt;G6D&gt;'FP&lt;H-!&amp;U!(!""*&lt;G2F?#"$&lt;WZO:7.U;7^O!!"I!0(8!V/L!!!!!QV65U)N34*$,GRW&lt;'FC$V641CV*-E-O&lt;(:D&lt;'&amp;T=QN65U)N34*$,G.U&lt;!!W1&amp;!!"Q!!!!)!"!!&amp;!!E!#Q!-(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!$1!!!!I!!!!!!!!!!1!!!!)!!!!$!!!!"!!!!!5!!!!'!!!!"```````````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!!!!!!!!!%!!!!!!!=!!!!!$A!41!5!$6.M98:F)%&amp;E:(*F=X-!$5!&amp;!!&gt;/&gt;7VF=GFD!"*!1!!"`````Q!""%*Z&gt;'5!!"N!&amp;A!"#W.P&lt;G:J:S"E982B!!:S:7:O&gt;7U!!%=!]&lt;GGFI1!!!!#%5Z*8UR71W^O:GFH,GRW&lt;'FC&amp;E.P&lt;G:J:S"%982B)&amp;*F:EZV&lt;3ZD&gt;'Q!&amp;E"Q!!%!!1!$"E.P&lt;G:J:Q!!%%!Q`````Q&gt;4:7.U;7^O!!^!"Q!)6'FN:3"0&gt;81!!"&gt;!"Q!12'6M98EA17:U:8)A5G6B:!!!&amp;U!(!"&amp;%:7RB?3"":H2F=C"8=GFU:1"?!0%!!!!!!!!!!QV65U)N34*$,GRW&lt;'FC$V641CV*-E-O&lt;(:D&lt;'&amp;T=RF$&lt;WZG;7&gt;V=G&amp;U;7^O,5.M&gt;8.U:8)O9X2M!"Z!5!!$!!9!"Q!)$5.P&lt;G:J:X6S982J&lt;WY!%%!Q`````Q:4&gt;(*J&lt;G=!!"B!1!!"`````Q!+#U.P&lt;GZF9X2J&lt;WZT!"&gt;!"Q!137ZE:8AA1W^O&lt;G6D&gt;'FP&lt;A!!;!$RVQ.8P!!!!!-.66.#,5ES1SZM&gt;GRJ9A^65U)N34*$,GRW9WRB=X-,66.#,5ES1SZD&gt;'Q!.E"1!!=!!!!#!!1!"1!*!!M!$"V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!U!!!!+!!!!!!!!!!%!!!!#!!!!!Q!!!!1!!!!&amp;!!!!"A!!!!=!!!!)!!!!#;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"=!A!!!!!!!!!!!!!!!!!!"!!!!!!!)!!!!!!]!%U!&amp;!!V4&lt;'&amp;W:3"":'2S:8.T!!V!"1!(4H6N:8*J9Q!31%!!!@````]!!12#?82F!!!&lt;1"9!!1ND&lt;WZG;7=A:'&amp;U91!'=G6G&lt;H6N!!"(!0'ZJJ;%!!!!!B&amp;/36^-6E.P&lt;G:J:SZM&gt;GRJ9B:$&lt;WZG;7=A2'&amp;U93"3:7:/&gt;7UO9X2M!":!=!!"!!%!!Q:$&lt;WZG;7=!!""!-0````](5W6D&gt;'FP&lt;A!01!=!#&amp;2J&lt;75A4X6U!!!81!=!%%2F&lt;'&amp;Z)%&amp;G&gt;'6S)&amp;*F971!!"&gt;!"Q!22'6M98EA17:U:8)A6X*J&gt;'5!8A$R!!!!!!!!!!-.66.#,5ES1SZM&gt;GRJ9A^65U)N34*$,GRW9WRB=X-:1W^O:GFH&gt;8*B&gt;'FP&lt;CV$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!!Q!'!!=!#!V$&lt;WZG;7&gt;V=G&amp;U;7^O!""!-0````]'5X2S;7ZH!!!91%!!!@````]!#AN$&lt;WZO:7.U;7^O=Q!81!=!%%FO:'6Y)%.P&lt;GZF9X2J&lt;WY!!"&gt;!"1!15'&amp;T=X&gt;P=G1A172E=G6T=Q!!;A$RVQ&gt;RK!!!!!-.66.#,5ES1SZM&gt;GRJ9A^65U)N34*$,GRW9WRB=X-,66.#,5ES1SZD&gt;'Q!/%"1!!A!!!!#!!1!"1!*!!M!$!!.(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!$A!!!!M!!!!!!!!!!1!!!!)!!!!$!!!!"!!!!!5!!!!'!!!!"Q!!!!A!!!!*`````[!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!(M!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"=!A!!!!!!!!!!!!!!!!!!"!!!!!!!*!!!!!"!!%U!&amp;!!V4&lt;'&amp;W:3"":'2S:8.T!!V!"1!(4H6N:8*J9Q!31%!!!@````]!!12#?82F!!!&lt;1"9!!1ND&lt;WZG;7=A:'&amp;U91!'=G6G&lt;H6N!!!31(!!!1!"!!-'1W^O:GFH!!!11$$`````"V.F9X2J&lt;WY!$U!(!!B5;7VF)%^V&gt;!!!&amp;U!(!""%:7RB?3"":H2F=C"3:7&amp;E!!!81!=!%52F&lt;'&amp;Z)%&amp;G&gt;'6S)&amp;&gt;S;82F!&amp;Y!]1!!!!!!!!!$$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T'5.P&lt;G:J:X6S982J&lt;WYN1WRV=X2F=CZD&gt;'Q!(E"1!!-!"A!(!!A.1W^O:GFH&gt;8*B&gt;'FP&lt;A!11$$`````"F.U=GFO:Q!!'%"!!!(`````!!I,1W^O&lt;G6D&gt;'FP&lt;H-!&amp;U!(!""*&lt;G2F?#"$&lt;WZO:7.U;7^O!!!81!5!%&amp;"B=X.X&lt;X*E)%&amp;E:(*F=X-!!!Z!)1B198.T&gt;W^S:!!!&lt;!$RVT12PQ!!!!-.66.#,5ES1SZM&gt;GRJ9A^65U)N34*$,GRW9WRB=X-,66.#,5ES1SZD&gt;'Q!/E"1!!E!!!!#!!1!"1!*!!M!$!!.!!Y&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!0!!!!$!!!!!!!!!!"!!!!!A!!!!-!!!!%!!!!"1!!!!9!!!!(!!!!#!!!!!E!!!!+`````[!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!(M!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!8!)!!!!!!!!!!!!!!!!!!!1!!!!!!#A!!!!!1!".!"1!.5WRB&gt;G5A172E=G6T=Q!.1!5!"UZV&lt;76S;7-!%E"!!!(`````!!%%1HFU:1!!%%!Q`````Q&gt;4:7.U;7^O!!^!"Q!)6'FN:3"0&gt;81!!"&gt;!"Q!12'6M98EA17:U:8)A5G6B:!!!&amp;U!(!"&amp;%:7RB?3"":H2F=C"8=GFU:1"?!0%!!!!!!!!!!QV65U)N34*$,GRW&lt;'FC$V641CV*-E-O&lt;(:D&lt;'&amp;T=RF$&lt;WZG;7&gt;V=G&amp;U;7^O,5.M&gt;8.U:8)O9X2M!"Z!5!!$!!1!"1!'$5.P&lt;G:J:X6S982J&lt;WY!%%!Q`````Q:4&gt;(*J&lt;G=!!"B!1!!"`````Q!)#U.P&lt;GZF9X2J&lt;WZT!"&gt;!"Q!137ZE:8AA1W^O&lt;G6D&gt;'FP&lt;A!!&amp;U!&amp;!""198.T&gt;W^S:#"":'2S:8.T!!!/1#%)5'&amp;T=X&gt;P=G1!!"N!&amp;A!"#W.P&lt;G:J:S"E982B!!:S:7:O&gt;7U!!%=!]&lt;GGFI1!!!!#%5Z*8UR71W^O:GFH,GRW&lt;'FC&amp;E.P&lt;G:J:S"%982B)&amp;*F:EZV&lt;3ZD&gt;'Q!&amp;E"Q!!%!!1!."E.P&lt;G:J:Q!!&lt;!$RVT13^Q!!!!-.66.#,5ES1SZM&gt;GRJ9A^65U)N34*$,GRW9WRB=X-,66.#,5ES1SZD&gt;'Q!/E"1!!E!!!!#!!-!"Q!*!!I!#Q!-!!Y&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!0!!!!$!!!!!!!!!!"!!!!!Q!!!!1!!!!&amp;!!!!"A!!!!=!!!!)!!!!#1!!!!I!!!!,`````[!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!?Q!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"=!A!!!!!!!!!!!!!!!!!!"!!!!!!!,!!!!!"%!%U!&amp;!!V4&lt;'&amp;W:3"":'2S:8.T!!V!"1!(4H6N:8*J9Q!31%!!!@````]!!12#?82F!!!11$$`````"V.F9X2J&lt;WY!$U!(!!B5;7VF)%^V&gt;!!!&amp;U!(!""%:7RB?3"":H2F=C"3:7&amp;E!!!81!=!%52F&lt;'&amp;Z)%&amp;G&gt;'6S)&amp;&gt;S;82F!&amp;Y!]1!!!!!!!!!$$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T'5.P&lt;G:J:X6S982J&lt;WYN1WRV=X2F=CZD&gt;'Q!(E"1!!-!"!!&amp;!!9.1W^O:GFH&gt;8*B&gt;'FP&lt;A!11$$`````"F.U=GFO:Q!!'%"!!!(`````!!A,1W^O&lt;G6D&gt;'FP&lt;H-!&amp;U!(!""*&lt;G2F?#"$&lt;WZO:7.U;7^O!!!81!5!%&amp;"B=X.X&lt;X*E)%&amp;E:(*F=X-!!!Z!)1B198.T&gt;W^S:!!!'U!7!!%,9W^O:GFH)'2B&gt;'%!"H*F:GZV&lt;1!!2Q$RO;;7B!!!!!)24EF@4&amp;:$&lt;WZG;7=O&lt;(:M;7)71W^O:GFH)%2B&gt;'%A5G6G4H6N,G.U&lt;!!71(!!!1!"!!U'1W^O:GFH!!"J!0%!!!!!!!!!!QV65U)N34*$,GRW&lt;'FC$V641CV*-E-O&lt;(:D&lt;'&amp;T=R&gt;198.T&gt;W^S:#"-:8:F&lt;#V&amp;&lt;H6N,G.U&lt;!!L1"9!!A&gt;-:8:F&lt;#!S"URF&gt;G6M)$%!$F"B=X.X&lt;X*E)%RF&gt;G6M!!"O!0(8ZB+@!!!!!QV65U)N34*$,GRW&lt;'FC$V641CV*-E-O&lt;(:D&lt;'&amp;T=QN65U)N34*$,G.U&lt;!!]1&amp;!!#A!!!!)!!Q!(!!E!#A!,!!Q!$A!0(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!%!!!!!U!!!!!!!!!!1!!!!)!!!!$!!!!"!!!!!5!!!!'!!!!"Q!!!!A!!!!*!!!!#A!!!!P`````I!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"\!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"=!A!!!!!!!!!!!!!!"!!!!$V641CV*-E-O&lt;(:D&lt;'&amp;T=Q</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Item Name="USB-I2C.ctl" Type="Class Private Data" URL="USB-I2C.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Accessor" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Byte" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Byte</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Byte</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read Byte.vi" Type="VI" URL="../Accessor/Byte Property/Read Byte.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;2!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!"1!(4H6N:8*J9Q!31%!!!@````]!"12#?82F!!!U1(!!(A!!(QV65U)N34*$,GRW&lt;'FC$V641CV*-E-O&lt;(:D&lt;'&amp;T=Q!,66.#,5ES1S"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!U1(!!(A!!(QV65U)N34*$,GRW&lt;'FC$V641CV*-E-O&lt;(:D&lt;'&amp;T=Q!+66.#,5ES1S"J&lt;A!!91$Q!!Q!!Q!%!!9!"Q!%!!1!"!!%!!A!"!!%!!E#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
			<Item Name="Write Byte.vi" Type="VI" URL="../Accessor/Byte Property/Write Byte.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;2!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!@$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T!!N65U)N34*$)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!V!"1!(4H6N:8*J9Q!31%!!!@````]!"Q2#?82F!!!U1(!!(A!!(QV65U)N34*$,GRW&lt;'FC$V641CV*-E-O&lt;(:D&lt;'&amp;T=Q!+66.#,5ES1S"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!)!!E#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!#%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">34078720</Property>
			</Item>
		</Item>
		<Item Name="Config" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Config</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Config</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read Config.vi" Type="VI" URL="../Accessor/Config Property/Read Config.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'5!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"N!&amp;A!"#W.P&lt;G:J:S"E982B!!:S:7:O&gt;7U!!%=!]&lt;GGFI1!!!!#%5Z*8UR71W^O:GFH,GRW&lt;'FC&amp;E.P&lt;G:J:S"%982B)&amp;*F:EZV&lt;3ZD&gt;'Q!&amp;E"Q!!%!!1!&amp;"E.P&lt;G:J:Q!!.%"Q!"Y!!"].66.#,5ES1SZM&gt;GRJ9A^65U)N34*$,GRW9WRB=X-!#V641CV*-E-A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.%"Q!"Y!!"].66.#,5ES1SZM&gt;GRJ9A^65U)N34*$,GRW9WRB=X-!#F641CV*-E-A;7Y!!'%!]!!-!!-!"!!'!!=!"!!%!!1!"!!)!!1!"!!*!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!I!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">268967936</Property>
			</Item>
			<Item Name="Write Config.vi" Type="VI" URL="../Accessor/Config Property/Write Config.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'5!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!@$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T!!N65U)N34*$)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"N!&amp;A!"#W.P&lt;G:J:S"E982B!!:S:7:O&gt;7U!!%=!]&lt;GGFI1!!!!#%5Z*8UR71W^O:GFH,GRW&lt;'FC&amp;E.P&lt;G:J:S"%982B)&amp;*F:EZV&lt;3ZD&gt;'Q!&amp;E"Q!!%!!1!("E.P&lt;G:J:Q!!.%"Q!"Y!!"].66.#,5ES1SZM&gt;GRJ9A^65U)N34*$,GRW9WRB=X-!#F641CV*-E-A;7Y!!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!#!!*!A!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!A!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!I!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">269230080</Property>
			</Item>
		</Item>
		<Item Name="Configuration" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Configuration:All</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Configuration</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read Configuration.vi" Type="VI" URL="../Accessor/Configuration Property/Read Configuration.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(.!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!"Q!)6'FN:3"0&gt;81!!"&gt;!"Q!12'6M98EA17:U:8)A5G6B:!!!&amp;U!(!"&amp;%:7RB?3"":H2F=C"8=GFU:1"?!0%!!!!!!!!!!QV65U)N34*$,GRW&lt;'FC$V641CV*-E-O&lt;(:D&lt;'&amp;T=RF$&lt;WZG;7&gt;V=G&amp;U;7^O,5.M&gt;8.U:8)O9X2M!"Z!5!!$!!5!"A!($5.P&lt;G:J:X6S982J&lt;WY!.%"Q!"Y!!"].66.#,5ES1SZM&gt;GRJ9A^65U)N34*$,GRW9WRB=X-!#V641CV*-E-A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.%"Q!"Y!!"].66.#,5ES1SZM&gt;GRJ9A^65U)N34*$,GRW9WRB=X-!#F641CV*-E-A;7Y!!'%!]!!-!!-!"!!)!!E!"!!%!!1!"!!+!!1!"!!,!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!Q!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
			<Item Name="Write Configuration.vi" Type="VI" URL="../Accessor/Configuration Property/Write Configuration.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(.!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!@$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T!!N65U)N34*$)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!^!"Q!)6'FN:3"0&gt;81!!"&gt;!"Q!12'6M98EA17:U:8)A5G6B:!!!&amp;U!(!"&amp;%:7RB?3"":H2F=C"8=GFU:1"?!0%!!!!!!!!!!QV65U)N34*$,GRW&lt;'FC$V641CV*-E-O&lt;(:D&lt;'&amp;T=RF$&lt;WZG;7&gt;V=G&amp;U;7^O,5.M&gt;8.U:8)O9X2M!"Z!5!!$!!=!#!!*$5.P&lt;G:J:X6S982J&lt;WY!.%"Q!"Y!!"].66.#,5ES1SZM&gt;GRJ9A^65U)N34*$,GRW9WRB=X-!#F641CV*-E-A;7Y!!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!#A!,!A!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!Q!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">34078720</Property>
			</Item>
		</Item>
		<Item Name="Connections" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Connections</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Connections</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read Connections.vi" Type="VI" URL="../Accessor/Connections Property/Read Connections.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;;!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-0````]'5X2S;7ZH!!!91%!!!@````]!"1N$&lt;WZO:7.U;7^O=Q!U1(!!(A!!(QV65U)N34*$,GRW&lt;'FC$V641CV*-E-O&lt;(:D&lt;'&amp;T=Q!,66.#,5ES1S"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!U1(!!(A!!(QV65U)N34*$,GRW&lt;'FC$V641CV*-E-O&lt;(:D&lt;'&amp;T=Q!+66.#,5ES1S"J&lt;A!!91$Q!!Q!!Q!%!!9!"Q!%!!1!"!!%!!A!"!!%!!E#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
			<Item Name="Write Connections.vi" Type="VI" URL="../Accessor/Connections Property/Write Connections.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;;!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!@$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T!!N65U)N34*$)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!""!-0````]'5X2S;7ZH!!!91%!!!@````]!"QN$&lt;WZO:7.U;7^O=Q!U1(!!(A!!(QV65U)N34*$,GRW&lt;'FC$V641CV*-E-O&lt;(:D&lt;'&amp;T=Q!+66.#,5ES1S"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!)!!E#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!#%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">34078720</Property>
			</Item>
		</Item>
		<Item Name="Delay After Read" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Configuration:Delay After Read</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Delay After Read</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read Delay After Read.vi" Type="VI" URL="../Accessor/Delay After Read Property/Read Delay After Read.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;*!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"&gt;!"Q!12'6M98EA17:U:8)A5G6B:!!!.%"Q!"Y!!"].66.#,5ES1SZM&gt;GRJ9A^65U)N34*$,GRW9WRB=X-!#V641CV*-E-A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.%"Q!"Y!!"].66.#,5ES1SZM&gt;GRJ9A^65U)N34*$,GRW9WRB=X-!#F641CV*-E-A;7Y!!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
			<Item Name="Write Delay After Read.vi" Type="VI" URL="../Accessor/Delay After Read Property/Write Delay After Read.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;*!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!@$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T!!N65U)N34*$)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"&gt;!"Q!12'6M98EA17:U:8)A5G6B:!!!.%"Q!"Y!!"].66.#,5ES1SZM&gt;GRJ9A^65U)N34*$,GRW9WRB=X-!#F641CV*-E-A;7Y!!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">34078720</Property>
			</Item>
		</Item>
		<Item Name="Delay After Write" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Configuration:Delay After Write</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Delay After Write</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read Delay After Write.vi" Type="VI" URL="../Accessor/Delay After Write Property/Read Delay After Write.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;*!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"&gt;!"Q!22'6M98EA17:U:8)A6X*J&gt;'5!.%"Q!"Y!!"].66.#,5ES1SZM&gt;GRJ9A^65U)N34*$,GRW9WRB=X-!#V641CV*-E-A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.%"Q!"Y!!"].66.#,5ES1SZM&gt;GRJ9A^65U)N34*$,GRW9WRB=X-!#F641CV*-E-A;7Y!!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
			<Item Name="Write Delay After Write.vi" Type="VI" URL="../Accessor/Delay After Write Property/Write Delay After Write.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;*!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!@$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T!!N65U)N34*$)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"&gt;!"Q!22'6M98EA17:U:8)A6X*J&gt;'5!.%"Q!"Y!!"].66.#,5ES1SZM&gt;GRJ9A^65U)N34*$,GRW9WRB=X-!#F641CV*-E-A;7Y!!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">34078720</Property>
			</Item>
		</Item>
		<Item Name="Index Connection" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Index Connection</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Index Connection</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read Index Connection.vi" Type="VI" URL="../Accessor/Select USB Property/Read Index Connection.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;$!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"&amp;!"Q!+5W6M:7.U)&amp;641A!!.%"Q!"Y!!"].66.#,5ES1SZM&gt;GRJ9A^65U)N34*$,GRW9WRB=X-!#V641CV*-E-A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.%"Q!"Y!!"].66.#,5ES1SZM&gt;GRJ9A^65U)N34*$,GRW9WRB=X-!#F641CV*-E-A;7Y!!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
			<Item Name="Write Index Connection.vi" Type="VI" URL="../Accessor/Select USB Property/Write Index Connection.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;$!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!@$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T!!N65U)N34*$)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"&amp;!"Q!+5W6M:7.U)&amp;641A!!.%"Q!"Y!!"].66.#,5ES1SZM&gt;GRJ9A^65U)N34*$,GRW9WRB=X-!#F641CV*-E-A;7Y!!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">34078720</Property>
			</Item>
		</Item>
		<Item Name="Password" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Password</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Password</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read Password.vi" Type="VI" URL="../Accessor/Password Property/Read Password.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;!!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!Z!)1B198.T&gt;W^S:!!!.%"Q!"Y!!"].66.#,5ES1SZM&gt;GRJ9A^65U)N34*$,GRW9WRB=X-!#V641CV*-E-A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.%"Q!"Y!!"].66.#,5ES1SZM&gt;GRJ9A^65U)N34*$,GRW9WRB=X-!#F641CV*-E-A;7Y!!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
			<Item Name="Write Password.vi" Type="VI" URL="../Accessor/Password Property/Write Password.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;!!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!@$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T!!N65U)N34*$)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!Z!)1B198.T&gt;W^S:!!!.%"Q!"Y!!"].66.#,5ES1SZM&gt;GRJ9A^65U)N34*$,GRW9WRB=X-!#F641CV*-E-A;7Y!!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">34078720</Property>
			</Item>
		</Item>
		<Item Name="Password Address" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Password Address</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Password Address</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read Password Address.vi" Type="VI" URL="../Accessor/Password Address Property/Read Password Address.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;*!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"&gt;!"1!15'&amp;T=X&gt;P=G1A172E=G6T=Q!!.%"Q!"Y!!"].66.#,5ES1SZM&gt;GRJ9A^65U)N34*$,GRW9WRB=X-!#V641CV*-E-A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.%"Q!"Y!!"].66.#,5ES1SZM&gt;GRJ9A^65U)N34*$,GRW9WRB=X-!#F641CV*-E-A;7Y!!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
			<Item Name="Write Password Address.vi" Type="VI" URL="../Accessor/Password Address Property/Write Password Address.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;*!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!@$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T!!N65U)N34*$)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"&gt;!"1!15'&amp;T=X&gt;P=G1A172E=G6T=Q!!.%"Q!"Y!!"].66.#,5ES1SZM&gt;GRJ9A^65U)N34*$,GRW9WRB=X-!#F641CV*-E-A;7Y!!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">34078720</Property>
			</Item>
		</Item>
		<Item Name="Password Level" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Password Level</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Password Level</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read Password Level.vi" Type="VI" URL="../Accessor/Password Level Property/Read Password Level.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'&lt;!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!'E!]1!!!!!!!!!$$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T&amp;V"B=X.X&lt;X*E)%RF&gt;G6M,56O&gt;7UO9X2M!#N!&amp;A!#"URF&gt;G6M)$)(4'6W:7QA-1!/5'&amp;T=X&gt;P=G1A4'6W:7Q!!$2!=!!?!!!@$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T!!N65U)N34*$)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$2!=!!?!!!@$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T!!J65U)N34*$)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
			<Item Name="Write Password Level.vi" Type="VI" URL="../Accessor/Password Level Property/Write Password Level.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'&lt;!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!@$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T!!N65U)N34*$)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!'E!]1!!!!!!!!!$$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T&amp;V"B=X.X&lt;X*E)%RF&gt;G6M,56O&gt;7UO9X2M!#N!&amp;A!#"URF&gt;G6M)$)(4'6W:7QA-1!/5'&amp;T=X&gt;P=G1A4'6W:7Q!!$2!=!!?!!!@$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T!!J65U)N34*$)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
		</Item>
		<Item Name="Section" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Section</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Section</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read Section.vi" Type="VI" URL="../Accessor/Section Property/Read Section.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;#!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-0````](5W6D&gt;'FP&lt;A!U1(!!(A!!(QV65U)N34*$,GRW&lt;'FC$V641CV*-E-O&lt;(:D&lt;'&amp;T=Q!,66.#,5ES1S"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!U1(!!(A!!(QV65U)N34*$,GRW&lt;'FC$V641CV*-E-O&lt;(:D&lt;'&amp;T=Q!+66.#,5ES1S"J&lt;A!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
			<Item Name="Write Section.vi" Type="VI" URL="../Accessor/Section Property/Write Section.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;#!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!@$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T!!N65U)N34*$)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!""!-0````](5W6D&gt;'FP&lt;A!U1(!!(A!!(QV65U)N34*$,GRW&lt;'FC$V641CV*-E-O&lt;(:D&lt;'&amp;T=Q!+66.#,5ES1S"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!#%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">34078720</Property>
			</Item>
		</Item>
		<Item Name="Slave Address" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Slave Address</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Slave Address</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read Slave Address.vi" Type="VI" URL="../Accessor/Slave Address Property/Read Slave Address.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;&amp;!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!".!"1!.5WRB&gt;G5A172E=G6T=Q!U1(!!(A!!(QV65U)N34*$,GRW&lt;'FC$V641CV*-E-O&lt;(:D&lt;'&amp;T=Q!,66.#,5ES1S"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!U1(!!(A!!(QV65U)N34*$,GRW&lt;'FC$V641CV*-E-O&lt;(:D&lt;'&amp;T=Q!+66.#,5ES1S"J&lt;A!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
			<Item Name="Write Slave Address.vi" Type="VI" URL="../Accessor/Slave Address Property/Write Slave Address.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;&amp;!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!@$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T!!N65U)N34*$)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!".!"1!.5WRB&gt;G5A172E=G6T=Q!U1(!!(A!!(QV65U)N34*$,GRW&lt;'FC$V641CV*-E-O&lt;(:D&lt;'&amp;T=Q!+66.#,5ES1S"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">34078720</Property>
			</Item>
		</Item>
		<Item Name="Time Out" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Configuration:Time Out</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Time Out</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read Time Out.vi" Type="VI" URL="../Accessor/Time Out Property/Read Time Out.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;"!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!"Q!)6'FN:3"0&gt;81!!$2!=!!?!!!@$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T!!N65U)N34*$)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$2!=!!?!!!@$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T!!J65U)N34*$)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
			<Item Name="Write Time Out.vi" Type="VI" URL="../Accessor/Time Out Property/Write Time Out.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;"!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!@$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T!!N65U)N34*$)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!^!"Q!)6'FN:3"0&gt;81!!$2!=!!?!!!@$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T!!J65U)N34*$)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">34078720</Property>
			</Item>
		</Item>
	</Item>
	<Item Name="Control" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Configuration-Cluster.ctl" Type="VI" URL="../Control/Configuration-Cluster.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#D!!!!"!!01!=!#&amp;2J&lt;75A4X6U!!!81!=!%%2F&lt;'&amp;Z)%&amp;G&gt;'6S)&amp;*F971!!"&gt;!"Q!22'6M98EA17:U:8)A6X*J&gt;'5!8A$R!!!!!!!!!!-.66.#,5ES1SZM&gt;GRJ9A^65U)N34*$,GRW9WRB=X-:1W^O:GFH&gt;8*B&gt;'FP&lt;CV$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!!Q!!!!%!!AV$&lt;WZG;7&gt;V=G&amp;U;7^O!!%!!Q!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074278912</Property>
		</Item>
		<Item Name="Direct Control-Cluster.ctl" Type="VI" URL="../Control/Direct Control-Cluster.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$8!!!!#1!.1!5!"U&amp;E:(*F=X-!$5!&amp;!!:%982B)$!!!!V!"1!'2'&amp;U93!R!!!.1!5!"E2B&gt;'%A-A!!$5!&amp;!!:%982B)$-!!!V!"1!'2'&amp;U93!U!!!+1#%&amp;5X2B=H1!$%!B"U2B&gt;'%A5F=!;Q$R!!!!!!!!!!-.66.#,5ES1SZM&gt;GRJ9A^65U)N34*$,GRW9WRB=X-;2'FS:7.U)%.P&lt;H2S&lt;WQN1WRV=X2F=CZD&gt;'Q!+E"1!!A!!!!"!!)!!Q!%!!5!"A!($E2J=G6D&gt;#"$&lt;WZU=G^M!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
		</Item>
		<Item Name="Password Level-Enum.ctl" Type="VI" URL="../Control/Password Level-Enum.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"R!!!!!1"J!0%!!!!!!!!!!QV65U)N34*$,GRW&lt;'FC$V641CV*-E-O&lt;(:D&lt;'&amp;T=R&gt;198.T&gt;W^S:#"-:8:F&lt;#V&amp;&lt;H6N,G.U&lt;!!L1"9!!A&gt;-:8:F&lt;#!S"URF&gt;G6M)$%!$F"B=X.X&lt;X*E)%RF&gt;G6M!!!"!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
		</Item>
	</Item>
	<Item Name="Method" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Get Connections.vi" Type="VI" URL="../Method/Get Connections.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;.!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-0````]'5X2S;7ZH!!!91%!!!@````]!"1N$&lt;WZO:7.U;7^O=Q!U1(!!(A!!(QV65U)N34*$,GRW&lt;'FC$V641CV*-E-O&lt;(:D&lt;'&amp;T=Q!,66.#,5ES1S"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!U1(!!(A!!(QV65U)N34*$,GRW&lt;'FC$V641CV*-E-O&lt;(:D&lt;'&amp;T=Q!+66.#,5ES1S"J&lt;A!!6!$Q!!Q!!Q!%!!9!"Q!%!!1!"!!%!!A!"!!%!!E#!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!*!!!!!!!1!+!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1090519168</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342714368</Property>
		</Item>
		<Item Name="Open.vi" Type="VI" URL="../Method/Open.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%F!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!@$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T!!N65U)N34*$)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$2!=!!?!!!@$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T!!J65U)N34*$)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
		</Item>
		<Item Name="Configuration.vi" Type="VI" URL="../Method/Configuration.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%F!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!@$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T!!N65U)N34*$)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$2!=!!?!!!@$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T!!J65U)N34*$)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
		</Item>
		<Item Name="Direct Control.vi" Type="VI" URL="../Method/Direct Control.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)"!!!!%A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!@$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T!!N65U)N34*$)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!V!"1!(172E=G6T=Q!.1!5!"E2B&gt;'%A-!!!$5!&amp;!!:%982B)$%!!!V!"1!'2'&amp;U93!S!!!.1!5!"E2B&gt;'%A-Q!!$5!&amp;!!:%982B)$1!!!J!)164&gt;'&amp;S&gt;!!-1#%(2'&amp;U93"36Q"L!0%!!!!!!!!!!QV65U)N34*$,GRW&lt;'FC$V641CV*-E-O&lt;(:D&lt;'&amp;T=RJ%;8*F9X1A1W^O&gt;(*P&lt;#V$&lt;(6T&gt;'6S,G.U&lt;!!K1&amp;!!#!!(!!A!#1!+!!M!$!!.!!Y/2'FS:7.U)%.P&lt;H2S&lt;WQ!!$2!=!!?!!!@$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T!!J65U)N34*$)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!]!%!-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!)!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!2!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
		</Item>
		<Item Name="Password.vi" Type="VI" URL="../Method/Password.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%S!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!@$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T!!N65U)N34*$)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$2!=!!?!!!@$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T!!J65U)N34*$)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
		<Item Name="Read.vi" Type="VI" URL="../Method/Read.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;=!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!$5!&amp;!!:3:7&amp;E)$)!!"*!1!!"`````Q!%"%*Z&gt;'5!!!1!!!!U1(!!(A!!(QV65U)N34*$,GRW&lt;'FC$V641CV*-E-O&lt;(:D&lt;'&amp;T=Q!,66.#,5ES1S"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!,1!5!"&amp;*F971!!!V!"1!(172E=G6T=Q!U1(!!(A!!(QV65U)N34*$,GRW&lt;'FC$V641CV*-E-O&lt;(:D&lt;'&amp;T=Q!+66.#,5ES1S"J&lt;A!!6!$Q!!Q!!Q!&amp;!!9!"Q!'!!9!"A!'!!A!#1!+!!M#!!"Y!!!.#!!!#1!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!)!!!!#!!!!*!!!!!!!1!-!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1094713472</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342976512</Property>
		</Item>
		<Item Name="Read Data By Slave Address.vi" Type="VI" URL="../Method/Read Data By Slave Address.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;2!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!$5!&amp;!!:3:7&amp;E)$)!!"*!1!!"`````Q!%"%*Z&gt;'5!!!1!!!!U1(!!(A!!(QV65U)N34*$,GRW&lt;'FC$V641CV*-E-O&lt;(:D&lt;'&amp;T=Q!,66.#,5ES1S"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!.1!=!"ERF&lt;G&gt;U;!!!.%"Q!"Y!!"].66.#,5ES1SZM&gt;GRJ9A^65U)N34*$,GRW9WRB=X-!#F641CV*-E-A;7Y!!&amp;1!]!!-!!-!"1!'!!=!"A!'!!9!"A!)!!E!"A!+!A!!?!!!$1A!!!E!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!#!!!!!!!!!#1!!!!!!%!#Q!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1090519168</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342714368</Property>
		</Item>
		<Item Name="Read Data By Target Address[].vi" Type="VI" URL="../Method/Read Data By Target Address[].vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;F!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"1!&amp;!!!31%!!!@````]!"!2#?82F!!!%!!!!.%"Q!"Y!!"].66.#,5ES1SZM&gt;GRJ9A^65U)N34*$,GRW9WRB=X-!#V641CV*-E-A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!$5!(!!:-:7ZH&gt;'A!!"R!1!!"`````Q!%$V2B=G&gt;F&gt;%&amp;E:(*F=X.&lt;81!U1(!!(A!!(QV65U)N34*$,GRW&lt;'FC$V641CV*-E-O&lt;(:D&lt;'&amp;T=Q!+66.#,5ES1S"J&lt;A!!6!$Q!!Q!!Q!&amp;!!9!"Q!'!!9!"A!'!!A!#1!+!!M#!!"Y!!!.#!!!#1!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!)!!!##!!!!*!!!!!!!1!-!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1090519168</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342714368</Property>
		</Item>
		<Item Name="Write Data By Slave Address.vi" Type="VI" URL="../Method/Write Data By Slave Address.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;/!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!@$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T!!N65U)N34*$)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"6!"1!/6'&amp;S:W6U)%&amp;E:(*F=X-!!"2!1!!"`````Q!("E*Z&gt;'6&lt;81!!.%"Q!"Y!!"].66.#,5ES1SZM&gt;GRJ9A^65U)N34*$,GRW9WRB=X-!#F641CV*-E-A;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!A!"!!*!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!##!!!!!!!!!#1!!!!!!%!#A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1090519168</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342714368</Property>
		</Item>
		<Item Name="Write Data By Target Address.vi" Type="VI" URL="../Method/Write Data By Target Address.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;&lt;!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!@$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T!!N65U)N34*$)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!^!"1!*172E=G6T=S!S!"*!1!!"`````Q!("%*Z&gt;'5!!"6!"A!/6'&amp;S:W6U)%&amp;E:(*F=X-!!$2!=!!?!!!@$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T!!J65U)N34*$)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!)!!E!#A)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!AA!!!!)!!!!E!!!!!!"!!M!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1090519168</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342714368</Property>
		</Item>
		<Item Name="Write.vi" Type="VI" URL="../Method/Write.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;4!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!@$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T!!N65U)N34*$)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!^!"1!*172E=G6T=S!S!"*!1!!"`````Q!("%*Z&gt;'5!!!V!"1!(172E=G6T=Q!U1(!!(A!!(QV65U)N34*$,GRW&lt;'FC$V641CV*-E-O&lt;(:D&lt;'&amp;T=Q!+66.#,5ES1S"J&lt;A!!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!#!!*!!I#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!))!!!!#!!!!*!!!!!!!1!,!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1090519168</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342714368</Property>
		</Item>
		<Item Name="Close.vi" Type="VI" URL="../Method/Close.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%F!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!@$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T!!N65U)N34*$)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$2!=!!?!!!@$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T!!J65U)N34*$)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
		</Item>
		<Item Name="Reset.vi" Type="VI" URL="../Method/Reset.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%F!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!@$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T!!N65U)N34*$)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$2!=!!?!!!@$6641CV*-E-O&lt;(:M;7)066.#,5ES1SZM&gt;G.M98.T!!J65U)N34*$)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
		</Item>
	</Item>
	<Item Name="SubVIs" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Get Password By Firmware Version.vi" Type="VI" URL="../SubVIs/Get Password By Firmware Version.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;]!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!&amp;5!&amp;!!Z598*H:81A172E=G6T=Q!!&amp;E"!!!(`````!!1)5'&amp;T=X&gt;P=G1!!!1!!!!U1(!!(A!!(QV65U)N34*$,GRW&lt;'FC$V641CV*-E-O&lt;(:D&lt;'&amp;T=Q!,66.#,5ES1S"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!.1!5!"F*F971A-A!!%E"!!!(`````!!E%1HFU:1!!.%"Q!"Y!!"].66.#,5ES1SZM&gt;GRJ9A^65U)N34*$,GRW9WRB=X-!#F641CV*-E-A;7Y!!'%!]!!-!!-!"1!'!!=!"A!'!!9!"A!)!!I!"A!,!Q!!?!!!$1A!!!E!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!##!!!!!!!!!!)!!!.!!!!$!!!!!!!!!!!!!!"!!Q!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
		</Item>
	</Item>
</LVClass>
